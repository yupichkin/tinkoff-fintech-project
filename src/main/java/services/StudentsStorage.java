package services;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import dto.Student;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@EnableScheduling
public class StudentsStorage {
    @Value("${students.storage.filename}")
    private String filename;
    @Value("${students.storage.default.minute}")
    private Integer defaultMinute;

    private List<Student> students = new ArrayList<>();


    @PostConstruct //after successfully creation bean - let's fill students from file resource
    private void fillStudents() {
        try (CSVReader csvReader = new CSVReader(new FileReader(filename))) {
            String [] nextLine;
            csvReader.readNext(); //miss first line with fields names - of course, we can hardcode the dynamic filling of fields,
            // but since the main task is to process the file into objects, I thought that this would be enough

            while ((nextLine = csvReader.readNext()) != null && nextLine.length == 4) { //just to be sure that line is correct
                Student student = new Student();
                student.setId(UUID.randomUUID());
                student.setName(nextLine[0]);
                student.setAge(Integer.valueOf(nextLine[1]));
                student.setClassTimeStart(LocalTime.of(Integer.parseInt(nextLine[2]),defaultMinute));
                student.setClassTimeFinish(LocalTime.of(Integer.parseInt(nextLine[3]),defaultMinute));
                students.add(student);
            }
        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
    }

    @CountStudents
    @Scheduled(fixedRate=1000*60*60) //every hour
    public List<Student> getBusyStudents() {
        //fillStudents(); //for now i think we need to think how to save UUIDs in memory or
        // file and then update students list every method recall
        LocalTime now = LocalTime.now();
        return students.stream().
                filter(s -> now.compareTo(s.getClassTimeStart()) > 0
                                && now.compareTo(s.getClassTimeFinish()) < 0)
                .collect(Collectors.toList());
    }
}
