package services;

import dto.Student;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
public class AspectCountStudent {
    private Map<String, Integer> countStudent = new HashMap<>();
    //now I identify students by name because for now i don't save their UUID anywhere.
    private Integer methodCallCounter = 0;

    @Around("@annotation(CountStudents)")
    public void logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        //long start = System.currentTimeMillis();
        List<Student> methodResult = (List<Student>) joinPoint.proceed();
        //long executionTime = System.currentTimeMillis() - start;
        //System.out.println(methodResult.size());
        methodResult.stream()
                .forEach(s -> countStudent.put(s.getName(),
                        countStudent.getOrDefault(s.getName(), 0) + 1));
        //System.out.println(countStudent.get("Борисов Илья Александрович"));
        //System.out.println(countStudent.size());

        System.out.println(joinPoint.getSignature() + " executed " + ++methodCallCounter + " times");
        System.out.println(countStudent);
    }
}