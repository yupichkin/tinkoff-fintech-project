import dto.Student;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class StudentsUtils {
    private final List<Student> students;

    public StudentsUtils(List<Student> students) {
        this.students = students;
    }

    public Set<String> getNamesAsSet() {
        return students.stream()
                .map(Student::getName)
                .collect(Collectors.toSet());
    }
    public Integer getAgeByName(String name) {
        return students.stream()
                .filter(student -> student.getName().equals(name))
                .map(Student::getAge)
                .reduce(0, Integer::sum);
    }

    public boolean isAnyOlderThanAge(int age) {
        return students.stream()
                .anyMatch(student -> student.getAge() > age);
    }

    public Map<UUID, String> getMapIdName() {
        return students.stream()
                .collect(Collectors.toMap(Student::getId, Student::getName));
    }

    public Map<Integer, List<Student>> getMapAgeStudents() {
        return students.stream()
                .collect(Collectors.groupingBy(Student::getAge));
    }
}
