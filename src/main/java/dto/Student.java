package dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private UUID id;
    private String name;
    private Integer age;
    private LocalTime classTimeStart;
    private LocalTime classTimeFinish;

    @Override
    public String toString() {
        return "id: " + this.id + " " +
                "name: " + this.name + " " +
                "age: " + this.age;
    }
}