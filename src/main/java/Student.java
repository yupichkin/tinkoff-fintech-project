
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class Student {
    private UUID id;
    private String name;
    private Integer age;

    @Override
    public String toString() {
        return "id: " + this.id + " " +
                "name: " + this.name + " " +
                "age: " + this.age;
    }
}