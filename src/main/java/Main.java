import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        Student student = new Student(UUID.randomUUID(), "oleg", 21);
        System.out.print("Created student ");
        System.out.println(student);
    }
}
