import dto.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test; //this is for running tests in IDEA interface
//import org.junit.Test;  //this is for running tests in EntryPoint main

import java.util.*;

public class StudentsUtilsTests {
    List<Student> students = createStudentList();
    StudentsUtils studentsUtils = new StudentsUtils(students);
    //расчитать суммарный возраст для определенного имени
    @Test
    public void testGetAgeByName() {
        //testing existed object
        System.out.print("testGetAgeByName");
        Integer age = studentsUtils.getAgeByName("Jame");
        Assertions.assertEquals(age, 23);
        age = studentsUtils.getAgeByName("yatorogod");
        Assertions.assertEquals(age, 17);
        age = studentsUtils.getAgeByName("7ckngMad");
        Assertions.assertEquals(age, 29);
        //testing two-times existed object
        age = studentsUtils.getAgeByName("b1t");
        Assertions.assertEquals(age, 56); //18+38
        //testing non-existed object
        age = studentsUtils.getAgeByName("random nickname");
        Assertions.assertEquals(age, 0); //should be - because we need calculate TOTAL age. So if it is zero - non existed students
        System.out.println(" - OK");
    }

    //получить Set, который содержит в себе только имена учеников
    @Test
    public void testGetNamesAsSet() {
        System.out.print("testGetNamesAsSet");
        Set<String> setA = new HashSet<>();
        setA.add("Jame");
        setA.add("mira");
        setA.add("qikert");
        setA.add("b1t");
        setA.add("yatorogod");
        setA.add("YEKINDAR");
        setA.add("7ckngMad");
        setA.add("N0tail");
        setA.add("ceh9");

        Set<String> setB = studentsUtils.getNamesAsSet();
        Assertions.assertEquals(setB,setA);
        System.out.println(" - OK");
    }

    //узнать, есть ли среди студентов кто-то старше заданного возраста
    @Test
    public void testIsAnyOlderThanAge() {
        System.out.print("testIsAnyOlderThanAge");
        Assertions.assertTrue(studentsUtils.isAnyOlderThanAge(20));
        Assertions.assertTrue(studentsUtils.isAnyOlderThanAge(37));
        Assertions.assertTrue(studentsUtils.isAnyOlderThanAge(20));
        Assertions.assertTrue(studentsUtils.isAnyOlderThanAge(17));
        Assertions.assertTrue(studentsUtils.isAnyOlderThanAge(16));

        Assertions.assertFalse(studentsUtils.isAnyOlderThanAge(38));
        Assertions.assertFalse(studentsUtils.isAnyOlderThanAge(39));
        Assertions.assertFalse(studentsUtils.isAnyOlderThanAge(100));
        Assertions.assertFalse(studentsUtils.isAnyOlderThanAge(100000));
        System.out.println(" - OK");
    }

    //преобразовать список в map ключ - id, значение - имя
    @Test
    public void testGetMapIdName() {
        System.out.print("testGetMapIdName");
        Map<UUID, String> mapA = new HashMap<>();
        mapA.put(students.get(0).getId(), "Jame");
        mapA.put(students.get(1).getId(), "mira");
        mapA.put(students.get(2).getId(), "qikert");
        mapA.put(students.get(3).getId(), "b1t");
        mapA.put(students.get(4).getId(), "b1t");
        mapA.put(students.get(5).getId(), "yatorogod");
        mapA.put(students.get(6).getId(), "YEKINDAR");
        mapA.put(students.get(7).getId(), "7ckngMad");
        mapA.put(students.get(8).getId(), "N0tail");
        mapA.put(students.get(9).getId(), "ceh9");
        Map<UUID, String> mapB = studentsUtils.getMapIdName();

        Assertions.assertEquals(mapB,mapA);
        System.out.println(" - OK");
    }

    //преобразовать список в map ключ - возраст, значение - коллекция объектов student с возрастов ключа
    @Test
    public void testGetMapAgeStudents() {
        System.out.print("testGetMapAgeStudents");
        Map<Integer, List<Student>> mapB = studentsUtils.getMapAgeStudents();

        Assertions.assertEquals(mapB.get(23), Arrays.asList(students.get(0), students.get(8)));
        Assertions.assertEquals(mapB.get(22), List.of(students.get(1)));
        Assertions.assertEquals(mapB.get(19), List.of(students.get(2)));
        Assertions.assertEquals(mapB.get(18), List.of(students.get(3)));
        Assertions.assertEquals(mapB.get(38), List.of(students.get(4)));
        Assertions.assertEquals(mapB.get(17), List.of(students.get(5)));
        Assertions.assertEquals(mapB.get(24), List.of(students.get(6)));
        Assertions.assertEquals(mapB.get(29), List.of(students.get(7)));
        Assertions.assertEquals(mapB.get(33), List.of(students.get(9)));
        System.out.println(" - OK");
    }

    List<Student> createStudentList() {
        List<Student> students = new ArrayList<>();
        students.add(new Student(UUID.randomUUID(), "Jame", 23));
        students.add(new Student(UUID.randomUUID(), "mira", 22));
        students.add(new Student(UUID.randomUUID(), "qikert", 19));
        students.add(new Student(UUID.randomUUID(), "b1t", 18));
        students.add(new Student(UUID.randomUUID(), "b1t", 38));
        students.add(new Student(UUID.randomUUID(), "yatorogod", 17));
        students.add(new Student(UUID.randomUUID(), "YEKINDAR", 24));
        students.add(new Student(UUID.randomUUID(), "7ckngMad", 29));
        students.add(new Student(UUID.randomUUID(), "N0tail", 23));
        students.add(new Student(UUID.randomUUID(), "ceh9", 33));

        return students;
    }
}
