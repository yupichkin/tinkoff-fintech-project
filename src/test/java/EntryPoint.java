import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;


public class EntryPoint {
    public static void main(String[] args) {
        JUnitCore junitCore = new JUnitCore();
        junitCore.addListener(new TextListener(System.out));
        junitCore.run(StudentsUtilsTests.class);
    }
}
