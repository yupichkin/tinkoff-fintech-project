# Tinkoff fintech project

project from learning tinkoff fintech course

## Experience
 Hello my name is Yampil. I have met with Java in 2018 and have been actively studying it for the last year and a half. Now my portfolio has 3 projects related to java core and 2 project that i've done by using spring boot.

 Spotify music advisor 

 - Console app implemented on Java.
 - Implemented sending requests and getting responses with REST api Spotify.
 - Realization authentication (oauth 2.0) to Spotify account.

Simple banking system

 - Console app implemented on Java
 - Implemented a simple database that stores user data.
 - The simplest sql queries - select, update, insert, delete.
 - Built SQL queries on prepared statements with savepoints

File Server

 - Console app implemented on Java
 - Implemented server and client that communicate and transfer data with each other via sockets and streams.
 - Serialization of an instance of a class that stores information about data stored on the server.
 - Multithreading is implemented - the server allocates a thread for each client.

I also as part of the summer practice, developed with team an api application that worked with the spotify api. The idea of   the application was to select the optimal set (playlist) of music based on several spotify accounts. For example, a group of friends gathered at a party and they want to turn on the music. But since each person has individual musical preferences, the problem arises of selecting music that everyone would like. Accordingly, our application gave a link for authorization in spotify as a input, and at the output it gave a link to the generated playlist in spotify. Since I was already working with the spotify api, I was working on authorizing and saving user data and their tokens in the database, because. our application would involve the generation of playlists from different groups and so that a person would not need to authenticate again - his token data was saved to the database.

After that, I also studied spring and did a couple of study projects.

Trade Helper (test-task for alfa-bank vacancy java-junior)

 - Spring app
 - Simple REST api with the necessary functionality
 - Implemented request to an external api for getting info about currency rates and getting a link to a GIF by tag .
 - Requests are accepted by the client using Spring Cloud OpenFeign.

Recipes 

 - Spring app
 - Simple REST api with the necessary functionality
 - Storage of recipes in the database for each user, identifying them via email that send as request body
 - Data storage security - only the creator of the recipe can delete and change his recipe.

After that, I was faced with the fact that in order to get a job as a java-junior, most vacancies already require experience in commercial development. Therefore, I decided to qualify for an internship / fintech courses at Tinkoff. Accordingly, first of all, I am motivated by the potential employment in Tinkoff and the opportunity to grow in it as a highly qualified specialist. To give my motivation in the form of an example - "I like java and spring" I consider bad motivation and I think that you need to be motivated for those things that will be constant over a long period of time.
